\babel@toc {brazil}{}
\beamer@sectionintoc {1}{Introdu\IeC {\c c}\IeC {\~a}o}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Raios c\IeC {\'o}smicos de alt\IeC {\'\i }ssimas energias}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Chuveiros atmosf\IeC {\'e}ricos extensos}{6}{0}{1}
\beamer@sectionintoc {2}{Motiva\IeC {\c c}\IeC {\~a}o}{17}{0}{2}
\beamer@subsectionintoc {2}{1}{D\IeC {\'e}ficit de m\IeC {\'u}ons}{18}{0}{2}
\beamer@subsectionintoc {2}{2}{Intera\IeC {\c c}\IeC {\~o}es hadr\IeC {\^o}nicas em chuveiros atmosf\IeC {\'e}ricos}{19}{0}{2}
\beamer@subsectionintoc {2}{3}{Multiplicidade em aceleradores}{20}{0}{2}
\beamer@sectionintoc {3}{An\IeC {\'a}lise dos observ\IeC {\'a}veis de chuveiros atmosf\IeC {\'e}ricos}{21}{0}{3}
\beamer@subsectionintoc {3}{1}{Densidade lateral de m\IeC {\'u}ons}{23}{0}{3}
\beamer@subsectionintoc {3}{2}{Desenvolvimento longitudinal}{25}{0}{3}
\beamer@subsectionintoc {3}{3}{N\IeC {\'u}mero de m\IeC {\'u}ons no n\IeC {\'\i }vel de observa\IeC {\c c}\IeC {\~a}o}{27}{0}{3}
\beamer@subsectionintoc {3}{4}{Produ\IeC {\c c}\IeC {\~a}o de m\IeC {\'u}ons em fun\IeC {\c c}\IeC {\~a}o da profundidade}{28}{0}{3}
\beamer@subsectionintoc {3}{5}{Resultados}{30}{0}{3}
\beamer@sectionintoc {4}{Distribui\IeC {\c c}\IeC {\~a}o de pseudorapidez e chuveiros atmosf\IeC {\'e}ricos}{34}{0}{4}
\beamer@subsectionintoc {4}{1}{Modelando a distribui\IeC {\c c}\IeC {\~a}o $dN/d\eta $ para primeira intera\IeC {\c c}\IeC {\~a}o}{36}{0}{4}
\beamer@subsectionintoc {4}{2}{Resultado sobre observ\IeC {\'a}veis de chuveiros atmosf\IeC {\'e}ricos}{40}{0}{4}
\beamer@subsectionintoc {4}{3}{Resultado}{44}{0}{4}
\beamer@sectionintoc {5}{Inclus\IeC {\~a}o de efeitos do detector}{49}{0}{5}
\beamer@subsectionintoc {5}{1}{Observat\IeC {\'o}rio Pierre Auger}{50}{0}{5}
\beamer@subsectionintoc {5}{2}{Simula\IeC {\c c}\IeC {\~a}o do arranjo de detectores}{54}{0}{5}
\beamer@subsectionintoc {5}{3}{Processo de reconstru\IeC {\c c}\IeC {\~a}o}{56}{0}{5}
\beamer@subsectionintoc {5}{4}{Cen\IeC {\'a}rio de teste}{58}{0}{5}
\beamer@sectionintoc {6}{Reconstru\IeC {\c c}\IeC {\~a}o de simula\IeC {\c c}\IeC {\~o}es de chuveiros atmosf\IeC {\'e}ricos}{65}{0}{6}
\beamer@subsectionintoc {6}{1}{Resultado}{69}{0}{6}
\beamer@sectionintoc {7}{Conclus\IeC {\~o}es}{71}{0}{7}
